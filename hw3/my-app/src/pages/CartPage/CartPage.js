import React, { useEffect, useState } from "react";
import CardsCantainer from "../../components/CardsContainer/CardsContainer";
import styles from './CartPage.module.scss'

const CartPage = () => {
    const [items, setItems] = useState(null);

    useEffect(() => {
        (async () => {
            const cards = await fetch ('./items.json').then(res => res.json())
            setItems(cards)
        })()
    }, []); 

    const findItemsInCart = () => {
        const itemsCart = JSON.parse(localStorage.getItem('cart'))
        return items.reduce((arrayCart, item) => {
            if (Array.isArray(itemsCart)) {
                if (itemsCart.includes(item.article)) {
                    arrayCart.push(item)
                }
            }
            return arrayCart
        }, []);
    }

    return(
        <>
        {items && <CardsCantainer items={findItemsInCart()} className={styles.cards}/>}
        </>
    )
}

export default CartPage;
import React, { useEffect, useState } from "react";
import CardsCantainer from "../../components/CardsContainer/CardsContainer";
import styles from './HomePage.module.scss'



const HomePage = () => {
    const [items, setItems] = useState(null);

    useEffect(() => {
        (async () => {
            const cards = await fetch ('./items.json').then(res => res.json());
            setItems(cards);
        })()
    }, []);

    return (
        <>
        {items && <CardsCantainer items={items} className={styles.cards}/>}
        </>
    )
}

export default HomePage;
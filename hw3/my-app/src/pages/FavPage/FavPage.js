import React, { useEffect, useState } from "react";
import CardsCantainer from "../../components/CardsContainer/CardsContainer";
import styles from './FavPage.module.scss'


const FavPage = () => {
    const [items, setItems] = useState(null);

    useEffect(() => {
        (async () => {
            const cards = await fetch ('./items.json').then(res => res.json());
            setItems(cards);
        })()
    }, []);

    const findFav = () => {
        const fav = JSON.parse(localStorage.getItem('fav'));
        return items.reduce((arrayFav, item) => {
             if (Array.isArray(fav)) {
                if (fav.includes(item.article)) {
                    arrayFav.push(item)
                }
            }
            return arrayFav;
        }, [])
    }

    return (
        <>
            {items && <CardsCantainer items={findFav()} className={styles.cards}/>}
        </>
    )
}

export default FavPage;
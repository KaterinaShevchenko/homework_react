import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router';
import HomePage from "../pages/HomePage/HomePage.js";
import CartPage from "../pages/CartPage/CartPage.js";
import FavPage from "../pages/FavPage/FavPage.js";


const Rout = () => {
    return (
        <Routes>
            <Route exact='true' path='/' element={<HomePage/>}/>
            <Route exact='true' path='/cart' element={<CartPage/>}/>
            <Route exact='true' path='/favourite' element={<FavPage/>}/>
      </Routes>
    )
}
export default Rout;

import styles from './App.module.scss';
import { BrowserRouter as Router } from 'react-router-dom'
import Header from './components/Header/Header';
import Footer from './components/Footer';
import Rout from './Routes/Routes';

function App() {
  return (
    <Router>
        <div className={styles.app}>
          <Header/>
          <Rout/>
          <Footer/>
        </div>
    </Router>
  );
}

export default App;

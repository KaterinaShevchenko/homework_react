import React from "react";
import styles from './Header.module.scss'
import { ReactComponent as Basket } from "../../assets/svg/basket.svg";
import { ReactComponent as Logo } from "../../assets/svg/logo.svg";
import { ReactComponent as FavHeader } from "../../assets/svg/favHeader.svg";
import { NavLink } from "react-router-dom";


const Header = () => {
    return (
        <header className={styles.header}>
        <div className={styles.header_container}>
            <NavLink exact="true"   to="/">{<Logo/>}</NavLink>
            <div className={styles.header_container_pages}>
                <NavLink exact="true"   to="/cart">{<Basket/>}</NavLink>
                <NavLink exact="true"  to="/favourite">{<FavHeader/>}</NavLink>  
            </div>
        </div>
    </header>
    )
}

export default Header;
import React from "react";
import styles from "./Footer.module.scss"

const Footer = () => {
    return (
        <footer className={styles.footer}>
            <p className={styles.footer_text}>@ https://dicentra.ua</p>
        </footer>
    )
}

export default Footer;
import React, { useState, useEffect } from 'react';
import styles from './Card.module.scss';
import Button from '../Button';
import PropTypes from 'prop-types';
import { ReactComponent as Fav } from "../../assets/svg/fav.svg";
import { ReactComponent as RemovaFav } from "../../assets/svg/removaFav.svg";
import { ReactComponent as Delete } from "../../assets/svg/delete.svg";
import Modal from '../Modal/Modal';


function Card(props) {
    const [favourite, setFavourite] = useState(false);
    const [modal, setModal] = useState(false);
    const [modalCart, setModalCart] = useState(false);
    const { item: {article, name, alt, price, path} } = props;

    useEffect(() => {
        const favorite = JSON.parse(localStorage.getItem('fav'));
        if (favorite) {
            favorite.forEach(item => {
                if (item === article) {
                    setFavourite(true);
                }
            })
        }
    }, []);
    const openModal = () => {
        setModal(true);
    }
    const openModalCart = () => {
        setModalCart(true);
    }
    const closeModal = () => {
        setModal(false);
    }
    const closeModalCart = () => {
        setModalCart(false);
    }

    const addFavorite = (article) => {
        if (localStorage.getItem('fav')) {
            const favorite = JSON.parse(localStorage.getItem('fav'));
          if (!favorite.includes(article)) {
            favorite.push(article);
            localStorage.setItem('fav', JSON.stringify(favorite));
          }
        } else {
          localStorage.setItem('fav', JSON.stringify([article]))
        }
        setFavourite(true);
    }

    const removaFromFavourite = (article) => { 
        if (localStorage.getItem('fav')) {
            const favorite = JSON.parse(localStorage.getItem('fav'));
            const newFavourite = favorite.map(item => {
                if (item !== article) {
                    return item; 
                }
            })
            const filter = newFavourite.filter(Number);
            localStorage.setItem('fav', JSON.stringify(filter));
            // console.log(document.location.pathname);
            if (document.location.pathname === '/favourite') {
                const itemToRemoveFromFav = document.getElementById(article);
                itemToRemoveFromFav.remove();
            }
        }
        setFavourite(false);
    }

    const addToCart = () => {
        if (localStorage.getItem('cart')) {
            const cart = JSON.parse(localStorage.getItem('cart'));
            if (!cart.includes(article)) {
                cart.push(article);
                localStorage.setItem('cart', JSON.stringify(cart));
            }
        } else {
            localStorage.setItem('cart', JSON.stringify([article]));
        }
        setModal(false);
    }

    const removeFromCart = () => {
        if (localStorage.getItem('cart')) {
            const cart = JSON.parse(localStorage.getItem('cart'));
            const newCart = cart.map(item => {
                if (item !== article) {
                    return item; 
                }
            })
            const filter = newCart.filter(Number);
            localStorage.setItem('cart', JSON.stringify(filter));
            if (document.location.pathname === '/cart') {
                const itemToRemoveFromCart = document.getElementById(article);
                itemToRemoveFromCart.remove();
            }
        }
    }

    const favIcon = () => {
        if (document.location.pathname === '/cart') {
            return <Delete onClick={() => { openModalCart() 
            }
            }/>
        } else {
            if (favourite) {
                return <RemovaFav onClick={()=>removaFromFavourite(article)}/>
            } else {
                return <Fav onClick={() => addFavorite(article)}/>
            }
        }
    }

    const btnAddToCart = React.useMemo(() => {
        if (document.location.pathname !== '/cart') {
            return <Button
            text='Add to cart'
            className={styles.item_content_btn}
            onClick={()=>openModal()}
            />
        }
    }, [])

    return (
        <div className={styles.item} id={article}>
        <div className={styles.item_content}>
           <h3>{name}</h3>
            <img src={path} alt={alt}/>
            <span className={styles.item_content_favourite}>
                {favIcon()}
            </span>
            <span className={styles.item_content_price}>Price: {price}</span>
            {btnAddToCart}
            {modal && (
                <Modal 
                    close={() => closeModal()}
                    text='Add item to cart?'
                    actions={[
                    <Button
                    key={2}
                    onClick={() => closeModal()}
                    className={styles.item_content_btn}
                    text='Cancel'
                    />,
                    <Button
                    className={styles.item_content_btn}
                    key={1}
                    onClick={() => addToCart(article)}
                    text='OK'
                    />
                    ]}
                />
                )}
                {modalCart && (<Modal
                    close={() => closeModalCart()}
                    text='Delete item from cart?'
                    actions={[
                    <Button
                    className={styles.item_content_btn}
                    key={1}
                    onClick={() => removeFromCart(article)}
                    text='Yes'
                    />,
                    <Button
                        key={2}
                        onClick={() => closeModalCart()}
                        className={styles.item_content_btn}
                        text='No'
                    />
                    ]}
                />
                )}
                    
        </div>
    </div> 
    )
}

Card.propTypes = {
    item: PropTypes.shape({
        name: PropTypes.string,
        path: PropTypes.string,
        alt: PropTypes.string,
        price: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number    
        ])
    })
}
Card.defaultProps = {
    item: null
}

export default Card;


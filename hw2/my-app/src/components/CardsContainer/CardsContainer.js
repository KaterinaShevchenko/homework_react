import React, { PureComponent } from 'react';
import styles from './CardsContainer.module.scss';
import PropTypes from 'prop-types';
// import Preloader from "../Preloader";
import Card from '../Card/Card';

class CardsContainer extends PureComponent {
    render() {
        const {items, click, clickCard} = this.props;

        return (
            <section className={styles.container}>
                <h1>Flowers</h1>
                <div className={styles.cards}>
                    {items.map((item) => <Card itemsCard={item} click={click} clickCard={clickCard} key={item.article}/>)}
                </div>
            </section>
        )
    }
}

CardsContainer.propTypes = {
    items: PropTypes.array
}

export default CardsContainer;
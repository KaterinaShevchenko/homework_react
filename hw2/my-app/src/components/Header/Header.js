import React, { PureComponent } from 'react';
import styles from './Header.module.scss';
import { ReactComponent as Basket } from "../../assets/svg/basket.svg";
import { ReactComponent as Logo } from "../../assets/svg/logo.svg";

class Header extends PureComponent {
    render() {
        return (
            <header className={styles.header}>
                <div className={styles.header_container}>
                    {<Logo/>}
                    {<Basket/>}
                </div>
            </header>
        )
    }
}
export default Header;
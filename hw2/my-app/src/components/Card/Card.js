import React, { PureComponent } from 'react';
import styles from './Card.module.scss';
import Button from '../Button';
import PropTypes from 'prop-types';
import { ReactComponent as Fav } from "../../assets/svg/fav.svg";
import Modal from '../Modal/Modal';
// import { ReactComponent as FavRemove } from "../../assets/svg/removaFav.svg";



class Card extends PureComponent {
    state = {
        modal: false,
    }
    clickModal = () => {
        this.setState({modal: !this.state.modal})
    }
  
    render() {
        const { modal } = this.state;
        const {click, clickCard, itemsCard, itemsCard: {name, path, article, price, alt}} = this.props;
        return (
            <>
            <div className={styles.item}>
                <div className={styles.item_content}>
                   <h3>{name}</h3>
                    <img src={path} alt={alt}/>
                    <span className={styles.item_content_favourite}>
                        <Fav onClick={()=>click(article)}/>
                    </span>
                    <span className={styles.item_content_price}>Price: {price}</span>
                    <Button
                        text='Add to cart'
                        className={styles.item_content_btn}
                        onClick={this.clickModal}
                    />
                </div>
            </div> 
            {modal && (
                <Modal 
                    close={this.clickModal}
                    text='Add item to cart?'
                    actions={[
                    <Button
                    className={styles.modal_button_container}
                    key={1}
                    onClick={()=>clickCard(article)}
                    text='OK'
                    />,
                    <Button
                        key={2}
                        onClick={this.clickModal}
                        className={styles.modal_button_container}
                        text='Cancel'
                    />
                    ]}
                />
                )}
            </>
        )
    }
}

Card.propTypes = {
    name: PropTypes.string,
    path: PropTypes.string,
    alt: PropTypes.string,
    price: PropTypes.string
}
Card.defaultProps = {
    name: '',
    path: '',
    alt: 'image',
    price: ''
}

export default Card;




// class Card extends PureComponent {
//     state = {
//         modal: false,
//     }
//     componentDidMount = () => {
//         const {itemsCard: {article}} = this.props;
//         const favorite = JSON.parse(localStorage.getItem('fav'));
//         const card = JSON.parse(localStorage.getItem('card'));
//         // if (favorite) {
//         //   this.setState({favorite: favorite});
//         // }
//         // if (card) {
//         //   this.setState({card: card})
//         // }
//     }
//     addToFav = (article) => {
//         const favorite = JSON.parse(localStorage.getItem('fav'));
//         if (favorite) {
//           if (!favorite.includes(article)) {
//             favorite.push(article);
//             localStorage.setItem('fav', JSON.stringify(favorite));
//           }
//         } else {
//           localStorage.setItem('fav', JSON.stringify([article]))
//         }
//         // console.log(JSON.parse(localStorage.getItem('fav')));
//         // console.log(this.state.favorite);
//       }
//     clickModal = () => {
//         this.setState({modal: !this.state.modal})
//       }
  
//     render() {
//         const { modal } = this.state;
//         const { clickCard, itemsCard, itemsCard: {name, path, article, price, alt,}} = this.props;

//         return (
//             <>
//             <div className={styles.item}>
//                 <div className={styles.item_content}>
//                    <h3>{name}</h3>
//                     <img src={path} alt={alt}/>
//                     <span className={styles.item_content_favourite}>
//                         <Fav onClick={this.addToFav(article)}/>
//                     </span>
//                     <span className={styles.item_content_price}>Price: {price}</span>
//                     <Button
//                         text='Add to cart'
//                         className={styles.item_content_btn}
//                         onClick={this.clickModal}
//                     />
//                 </div>
//             </div> 
//             {modal && (
//                 <Modal 
//                     close={this.clickModal}
//                     text='Add item to cart?'
//                     actions={[
//                     <Button
//                     className={styles.modal_button_container}
//                     key={1}
//                     onClick={() => clickCard(article)}
//                     text='OK'
//                     />,
//                     <Button
//                         key={2}
//                         onClick={this.clickModal}
//                         className={styles.modal_button_container}
//                         text='Cancel'
//                     />
//                     ]}
//                 />
//                 )}
//             </>
//         )
//     }
// }

// Card.propTypes = {
//     name: PropTypes.string,
//     path: PropTypes.string,
//     alt: PropTypes.string,
//     price: PropTypes.string
// }
// Card.defaultProps = {
//     name: '',
//     path: '',
//     alt: 'image',
//     price: ''
// }

// export default Card;
import React, { PureComponent } from 'react';
import styles from './Modal.module.scss';
import PropTypes from 'prop-types';

class Modal extends PureComponent {
    render() {
        const {close, text, actions} = this.props;
        return (
            <div className={styles.modal_bg} onClick={close}>
                <div onClick={(e)=> {e.stopPropagation()}}  className={styles.modal_body}>
                    <div className={styles.modal_header}>
                        <div onClick={close} className={styles.modal_close}>
                            &times;
                        </div>
                    </div>
                    <div className={styles.modal_content}>
                        <p>{text}</p>
                        <div className={styles.modal_footer_btn}>{actions}</div>
                    </div>
                </div>
            </div>
        )
    }
}
Modal.propTypes = {
    close: PropTypes.func,
    text: PropTypes.string,
    actions: PropTypes.array
}
// Modal.defaultProps = {
//     close: ()={}
// }

export default Modal;
import React, {PureComponent} from 'react';
// import styles from './App.module.scss';
import Header from './components/Header';
import CardsContainer from './components/CardsContainer/CardsContainer';
// import Modal from './components/Modal/Modal';
// import Button from './components/Button';
// import axios from 'axios'


class App extends PureComponent {
  state = {
    items: [],
    favorite: [],
    card: [],
    modal: false
  }
  async componentDidMount() {
    const items = await fetch ('./items.json').then(res => res.json());
    this.setState({items})
    const favorite = JSON.parse(localStorage.getItem('fav'));
    const card = JSON.parse(localStorage.getItem('card'));
    if (favorite) {
      this.setState({favorite: favorite});
    }
    if (card) {
      this.setState({card: card})
    }
    console.log(favorite);
  }
  addToFav = (article) => {
    const favorite = JSON.parse(localStorage.getItem('fav'));
    if (favorite) {
      if (!favorite.includes(article)) {
        favorite.push(article);
        localStorage.setItem('fav', JSON.stringify(favorite));
      }
    } else {
      localStorage.setItem('fav', JSON.stringify([article]))
    }
    // console.log(JSON.parse(localStorage.getItem('fav')));
    // console.log(this.state.favorite);
  }
  addToCart = (article) => {
    const card = JSON.parse(localStorage.getItem('card'));
    if (card) {
      if (!card.includes(article)) {
        card.push(article);
        localStorage.setItem('card', JSON.stringify(card));
      }
    } else {
      localStorage.setItem('card', JSON.stringify([article]))
    }
  }

  render() {
    const {items} = this.state;
    return (
      <>
        <Header title="Shop"/>
        <CardsContainer items={items} key={items.id} click={this.addToFav} clickCard={this.addToCart}/>
      </>
    );
  }
}

export default App;

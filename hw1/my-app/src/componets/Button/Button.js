import React, {Component} from 'react'
import styles from './Button.module.scss';

class Button extends Component {
    render() {
        const { backgroundColor, text, onClick, className, classNameContainer} = this.props;
        return (
            <div className={classNameContainer}>
                <button 
                    style={{backgroundColor: backgroundColor}}
                    className = {className}
                    onClick = {onClick}
                >
                    {text}
                </button>
            </div>
        )
    }   
}





// class Button extends Component {
//     render() {
//         const forbtn = [
//             { backgroundcolor: '#F2BED8', text: 'Open first modal', onClick: '', id: '1'},
//             { backgroundcolor: '#BEF2F2', text: 'Open second modal', onClick: '', id: '2'}
//         ]
//         return (
//             <div className="container_btn">
//                 {forbtn.map(btn => {
//                     return (
//                         <button style={{backgroundColor: btn.backgroundcolor}} key={btn.id}>{btn.text}</button>
//                     )
//                 })}
//             </div>
//         )
//     }   
// }
export default Button;


import React, {Component} from 'react'
import styles from './Modal.module.scss';

export default class Modal extends Component {
    render () {
        const {close, header, closeButton, text, actions} = this.props;

        return (
            <div className={styles.modal_bg} onClick={close}>
                <div onClick={(e)=> {e.stopPropagation()}}  className={styles.modal_body}>
                    <div className={styles.modal_header}>
                        <h2>
                            {header}
                        </h2>
                        {closeButton && (
                            <div onClick={close} className={styles.modal_close}>
                                &times;
                            </div>
                        )}
                    </div>
                    <div>
                        <p>{text}</p>
                        <div className={styles.modal_footer_btn}>{actions}</div>
                    </div>
                </div>
            </div>
        )
    }
}
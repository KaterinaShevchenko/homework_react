import React, {Component} from 'react';
import Button from "./componets/Button/Button";
import Modal from "./componets/Modal/Modal"
import styles from './App.module.scss';


class App extends Component  {
  state = {
    modalFirst: false,
    modalSecond: false
  }
  clickFirstModal = () => {
    this.setState({modalFirst: !this.state.modalFirst})
  }
  clickSecondModal = () => {
    this.setState({modalSecond: !this.state.modalSecond})
  }
  // openFirstModal = () => {
  //   this.setState({firstModal: true});
  // }
  // closeFirstModal = () => {
  //   this.setState({firstModal: false});
  // }
  // openSecondModal = () => {
  //   this.setState({secondModal: true});
  // }
  // closeSecondModal = () => {
  //   this.setState({secondModal: false});
  // }
  
  render() {
    const {modalFirst, modalSecond} = this.state;
    return (
      <div className={styles.container}>
        <div className={styles.container_btn}>
          <Button 
            backgroundColor='#D7D7D7'
            text='Open first modal'
            className={styles.button}
            onClick={this.clickFirstModal}
          />
          <Button 
            backgroundColor='#D7D7D7'
            text='Open second modal'
            className={styles.button}
            onClick={this.clickSecondModal}
          />
        </div>
        {modalFirst && (<Modal
        closeButton={true}
        close={this.clickFirstModal}
        text="Some text"
        header="First modal header"
        actions={[
          <Button
          classNameContainer={styles.modal_button_container}
          key={1}
          onClick={this.clickFirstModal}
          className={styles.button}
          text='Ok'
          />,
          <Button
              key={2}
              onClick={this.clickFirstModal}
              className={styles.button}
              text='Cancel'
          />
        ]}
        />)}
        {modalSecond && (<Modal
        closeButton={true}
        close={this.clickSecondModal}
        text="Some text"
        header="Second modal header"
        actions={[
          <Button
          classNameContainer={styles.modal_button_container}
          key={1}
          onClick={this.clickSecondModal}
          className={styles.button}
          text='OK'
          />,
          <Button
              key={2}
              onClick={this.clickSecondModal}
              className={styles.button}
              text='Cancel'
          />
        ]}
        />)}
      </div>
    )
  }   
}

export default App;

// function App() {
//   const forbtn = [
//     { backgroundcolor: '#F2BED8', text: 'Open first modal', onClick: '', id: '1'},
//     { backgroundcolor: '#BEF2F2', text: 'Open second modal', onClick: '', id: '2'}
// ]
//     return (
//       <div>
//       <Button forbtn={forbtn}/>
//     </div>
//     )
// }

// export default App;

import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import CardsCantainer from "../../components/CardsContainer/CardsContainer";
import { getItems } from "../../store/actionsCreators/itemsActionCreators";
import styles from './FavPage.module.scss'


const FavPage = () => {
    const dispatch = useDispatch();
    const items = useSelector(({items}) => items.items);

    useEffect(() => {
        dispatch(getItems());
    }, []);

    const findFav = () => {
        const fav = JSON.parse(localStorage.getItem('fav'));
        return items.reduce((arrayFav, item) => {
             if (Array.isArray(fav)) {
                if (fav.includes(item.article)) {
                    arrayFav.push(item)
                }
            }
            return arrayFav;
        }, [])
    }

    return (
        <>
            {items && <CardsCantainer items={findFav()} className={styles.cards}/>}
        </>
    )
}

export default FavPage;
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import CardsCantainer from "../../components/CardsContainer/CardsContainer";
import { getItems } from "../../store/actionsCreators/itemsActionCreators";
import styles from './CartPage.module.scss'

const CartPage = () => {
    const dispatch = useDispatch();
    const items = useSelector(({items}) => items.items)

    useEffect(() => {
        dispatch(getItems);
    }, []); 

    const findItemsInCart = () => {
        const itemsCart = JSON.parse(localStorage.getItem('cart'))
        return items.reduce((arrayCart, item) => {
            if (Array.isArray(itemsCart)) {
                if (itemsCart.includes(item.article)) {
                    arrayCart.push(item)
                }
            }
            return arrayCart
        }, []);
    }

    return(
        <>
        {items && <CardsCantainer items={findItemsInCart()} className={styles.cards}/>}
        </>
    )
}

export default CartPage;
import React, { useState, useEffect } from 'react';
import styles from './Card.module.scss';
import Button from '../Button';
import PropTypes from 'prop-types';
import { ReactComponent as Fav } from "../../assets/svg/fav.svg";
import { ReactComponent as RemovaFav } from "../../assets/svg/removaFav.svg";
import { ReactComponent as Delete } from "../../assets/svg/delete.svg";
import Modal from '../Modal/Modal';
import { useDispatch, useSelector } from 'react-redux';
import { GET_ITEMS } from '../../store/actions/itemsAction';
import { OPEN_MODAL_CART, CLOSE_MODAL_CART, OPEN_MODAL_DELETE, CLOSE_MODAL_DELETE } from '../../store/actions/modalAction';


function Card(props) {
    const [favourite, setFavourite] = useState(false);
    const { item: {article, name, alt, price, path} } = props;

    const dispatch = useDispatch()
    const modalCar = useSelector((state) => state.modal.modalCart);
    const modalDelete = useSelector((state) => state.modal.modalDelete);

    useEffect(() => {
        const favorite = JSON.parse(localStorage.getItem('fav'));
        if (favorite) {
            favorite.forEach(item => {
                if (item === article) {
                    setFavourite(true);
                }
            })
        }
    }, [article]);
        
    const openModalCart = () => {
        dispatch({type: OPEN_MODAL_CART});
    }
    const closeModalCart = () => {
        dispatch({type: CLOSE_MODAL_CART});
    }
    const openModalDelete = () => {
        dispatch({type: OPEN_MODAL_DELETE});
    }
    const closeModalDelete = () => {
        dispatch({type: CLOSE_MODAL_DELETE});
    }

    const addFavorite = (article) => {
        if (localStorage.getItem('fav')) {
            const favorite = JSON.parse(localStorage.getItem('fav'));
          if (!favorite.includes(article)) {
            favorite.push(article);
            localStorage.setItem('fav', JSON.stringify(favorite));
          }
        } else {
          localStorage.setItem('fav', JSON.stringify([article]))
        }
        setFavourite(true);
    }

    const removaFromFavourite = (article) => { 
        if (localStorage.getItem('fav')) {
            const favorite = JSON.parse(localStorage.getItem('fav'));
            const newFavourite = favorite.map(item => {
                if (item !== article) {
                    return item; 
                }
            })
            const filter = newFavourite.filter(Number);
            localStorage.setItem('fav', JSON.stringify(filter));
            // console.log(document.location.pathname);
            if (document.location.pathname === '/favourite') {
                const itemToRemoveFromFav = document.getElementById(article);
                itemToRemoveFromFav.remove();
            }
        }
        setFavourite(false);
    }

    const addToCart = () => {
        if (localStorage.getItem('cart')) {
            const cart = JSON.parse(localStorage.getItem('cart'));
            if (!cart.includes(article)) {
                cart.push(article);
                localStorage.setItem('cart', JSON.stringify(cart));
            }
        } else {
            localStorage.setItem('cart', JSON.stringify([article]));
        }
        closeModalCart()
    }

    const removeFromCart = () => {
        if (localStorage.getItem('cart')) {
            const cart = JSON.parse(localStorage.getItem('cart'));
            const newCart = cart.map(item => {
                if (item !== article) {
                    return item; 
                }
            })
            const filter = newCart.filter(Number);
            localStorage.setItem('cart', JSON.stringify(filter));
            if (document.location.pathname === '/cart') {
                const itemToRemoveFromCart = document.getElementById(article);
                itemToRemoveFromCart.remove();
            }
        }
        closeModalDelete()
    }

    const favIcon = () => {
        if (document.location.pathname === '/cart') {
            return <Delete onClick={() => { openModalDelete() }}/>
        } else {
            if (favourite) {
                return <RemovaFav onClick={()=>removaFromFavourite(article)}/>
            } else {
                return <Fav onClick={() => addFavorite(article)}/>
            }
        }
    }

    const btnAddToCart = React.useMemo(() => {
        if (document.location.pathname !== '/cart') {
            return <Button
            text='Add to cart'
            className={styles.item_content_btn}
            onClick={()=>openModalCart()}
            />
        }
    }, [])

    return (
        <div className={styles.item} id={article}>
        <div className={styles.item_content}>
           <h3>{name}</h3>
            <img src={path} alt={alt}/>
            <span className={styles.item_content_favourite}>
                {favIcon()}
            </span>
            <span className={styles.item_content_price}>Price: {price}</span>
            {btnAddToCart}
            {modalCar && (
                <Modal 
                    close={() => closeModalCart()}
                    text='Add item to cart?'
                    actions={[
                    <Button
                    key={2}
                    onClick={() => closeModalCart()}
                    className={styles.item_content_btn}
                    text='Cancel'
                    />,
                    <Button
                    className={styles.item_content_btn}
                    key={1}
                    onClick={() => addToCart(article)}
                    text='OK'
                    />
                    ]}
                />
                )}
                {modalDelete && (<Modal
                    close={() => closeModalDelete()}
                    text='Delete item from cart?'
                    actions={[
                    <Button
                    className={styles.item_content_btn}
                    key={1}
                    onClick={() => removeFromCart(article)}
                    text='Yes'
                    />,
                    <Button
                        key={2}
                        onClick={() => closeModalDelete()}
                        className={styles.item_content_btn}
                        text='No'
                    />
                    ]}
                />
                )}
                    
        </div>
    </div> 
    )
}

Card.propTypes = {
    item: PropTypes.shape({
        name: PropTypes.string,
        path: PropTypes.string,
        alt: PropTypes.string,
        price: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number    
        ])
    })
}
Card.defaultProps = {
    item: null
}

export default Card;
import React, { PureComponent } from 'react';
import styles from './Modal.module.scss';
import PropTypes from 'prop-types';
import { ReactComponent as Close } from "../../assets/svg/close.svg";

class Modal extends PureComponent {
    render() {
        const {close, text, actions} = this.props;
        
        return (
            <div className={styles.modal_bg} onClick={close}>
                <div onClick={(e)=> {e.stopPropagation()}}  className={styles.modal_body}>
                    <div className={styles.modal_header}>
                        <div onClick={close} className={styles.modal_close}>
                            <Close/>
                        </div>
                    </div>
                    <div className={styles.modal_content}>
                        <p className={styles.modal_content_text}>{text}</p>
                        <div className={styles.modal_footer_btn}>{actions}</div>
                    </div>
                </div>
            </div>
        )
    }
}
Modal.propTypes = {
    close: PropTypes.func,
    text: PropTypes.string,
    actions: PropTypes.array
}
// Modal.defaultProps = {
//     close: ()={}
// }

export default Modal;
import { OPEN_MODAL_CART, CLOSE_MODAL_CART, OPEN_MODAL_DELETE, CLOSE_MODAL_DELETE } from "../actions/modalAction";

export const openModalCart = () => ({ type: OPEN_MODAL_CART});
export const closeModalCaart = () => ({ type: CLOSE_MODAL_CART});
export const openModalDetete = () => ({ type: OPEN_MODAL_DELETE});
export const closeModalDetete = () => ({ type: CLOSE_MODAL_DELETE});

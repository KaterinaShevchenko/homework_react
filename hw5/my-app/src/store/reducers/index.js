import { combineReducers } from "redux";
import itemReducer from "./itemsReducer";
import modalReducer from "./modalReducer";

const appReducer = combineReducers({
    items: itemReducer, 
    modal: modalReducer
})

export default appReducer;
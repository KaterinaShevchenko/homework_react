import { OPEN_MODAL_CART, CLOSE_MODAL_CART, OPEN_MODAL_DELETE, CLOSE_MODAL_DELETE } from "../actions/modalAction";

const initialState = {
    modalCart: false,
    modalDelete: false
  }
  

const modalReducer = (state = initialState, action) => {
    switch (action.type) {
        case OPEN_MODAL_CART: {
            return {...state, modalCart: true}
        }
        case CLOSE_MODAL_CART: {
            return {...state, modalCart: false}
        }
        case OPEN_MODAL_DELETE: {
            return {...state, modalDelete: true}
        }
        case CLOSE_MODAL_DELETE: {
            return {...state, modalDelete: false}
        }
        default: {
            return state;
        }
    }
}

export default modalReducer;
import { GET_ITEMS } from "../actions/itemsAction";

const initialState = {
    items: []
  }
  

const itemReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ITEMS: {
            return {...state, items: action.payload}
        }
        default: {
            return state;
        }
    }
}

export default itemReducer;
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import CardsCantainer from "../../components/CardsContainer/CardsContainer";
import styles from './HomePage.module.scss'
import { getItems } from "../../store/actionsCreators/itemsActionCreators";



const HomePage = () => {
    const dispatch = useDispatch();
    const items = useSelector(({items}) => items.items);

    useEffect(() => {
        dispatch(getItems());
    }, []);

    return (
        <>
        {items && <CardsCantainer items={items} className={styles.cards}/>}
        </>
    )
}

export default HomePage;
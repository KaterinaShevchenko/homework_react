import styles from './App.module.scss';
import { BrowserRouter as Router } from 'react-router-dom'
import Header from './components/Header/Header';
import Footer from './components/Footer';
import Rout from './Routes/Routes';
import {Provider} from 'react-redux'
import store from './store';





function App() {
  return (
    <Provider store={store}>
          <Router>
            <div className={styles.app}>
              <Header/>
              <Rout/>
              <Footer/>
            </div>
          </Router>
    </Provider>
  );
}

export default App;

import React from "react";
import styles from "./CardsContainer.module.scss"
import Card from "../Card/Card";
import PropTypes from 'prop-types';

function CardsCantainer(props) {
    const { items, className } = props;

    const title = React.useMemo(() => {
        if (document.location.pathname === '/cart') {
            return <h1>Items in your cart</h1>
        } else if (document.location.pathname === '/favourite') {
                return <h1>Your favourite items</h1> 
        } else {
            return <h1>Flowers</h1>
        }
    }, []);

    return(
            <main className={styles.container}>
                {title}
                <div className={className}>
                    {items.map((item) => 
                    <Card item={item} key={item.article} id={item.articl}/>
                    )}
                </div>
            </main>
    )
} 

CardsCantainer.propTypes = {
    items: PropTypes.array.isRequired,
    className: PropTypes.string

}

export default CardsCantainer;



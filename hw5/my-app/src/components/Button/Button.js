import React, { PureComponent } from 'react';
import styles from './Button.module.scss';
import PropTypes from 'prop-types';

class Button extends PureComponent {
    render() {
        const { text, onClick, className, type} = this.props;

        return (
            <button 
                className = {className}
                onClick = {onClick}
                type= {type}
            >
                {text}
            </button>
        )
    }   
}

Button.propTypes = {
    text: PropTypes.string,
    onClick: PropTypes.func,
    className: PropTypes.string,
    type: PropTypes.oneOf(['submit', 'button'])
}
Button.defaultProps = {
    text: '',
    type: 'button',
    className: '',
    // onClick: ()={}
}

export default Button;